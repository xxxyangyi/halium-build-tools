set -ex

device=$1
lunch=$2
ver=$3

cd $ver

rm -f .repo/local_manifests/device.xml
repo sync -j10 -c --force-sync --force-remove-dirty
JOBS=10 ./halium/devices/setup $device --force-sync --force-remove-dirty

export LC_ALL=C
export USE_CCACHE=1
source build/envsetup.sh
lunch $lunch-userdebug
time make clobber
rm -rf out/target/product/$device/images || true
time make -j10
time make halium-boot -j10
mkdir out/target/product/$device/images
mv out/target/product/$device/*.img out/target/product/$device/images/

lunch $lunch-eng
time make recoveryimage -j10
mv out/target/product/$device/recovery.img out/target/product/$device/images/recovery-unlocked.img
