set -ex

device=$1
output=$2
ver=$3
dir=out/target/product/$device/images

cd $ver

echo "Working on device: $device"
if [ ! -f "$dir/halium-boot.img" ]; then
    echo "halium-boot.img does not exist!"
exit 1; fi
if [ ! -f "$dir/recovery.img" ]; then
    echo "recovery.img does not exist!"
exit 1; fi
if [ ! -f "$dir/system.img" ]; then
    echo "system.img does not exist!"
exit 1; fi
wDir=$(mktemp -d /tmp/ota.XXXXXXXX)
mkdir $wDir/partitions
cp $dir/halium-boot.img $wDir/partitions/boot.img
cp $dir/recovery.img $wDir/partitions
# Copy common device-files first so if there is some device spesific changes it will override the common ones
mkdir -p $wDir/system/var/lib/lxc/android/

## SPARSE FILE TRANSLATION
# Needed with Halium-7.1 builds mostly
fileType=$(file -b0 $dir/system.img)
if [[ $fileType == "Android sparse image"* ]]; then
    echo "Converting sparse image to image"
    mv $dir/system.img $dir/system.sparse.img
    simg2img $dir/system.sparse.img $dir/system.img
    e2fsck -fy $dir/system.img >/dev/null
    resize2fs -p -M $dir/system.img
fi

cp $dir/system.img $wDir/system/var/lib/lxc/android/
tar cfJ "$output/halium_"$device".tar.xz" -C $wDir partitions/ system/
echo "$(date +%Y%m%d)-$RANDOM" > "$output/halium_"$device".tar.build"

# Also move recovery and bootimg to outdir
cp $dir/recovery-unlocked.img "$output/halium-unlocked-recovery_"$device".img"
cp $dir/recovery.img "$output/halium-locked-recovery_"$device".img"
cp $dir/halium-boot.img "$output/halium-boot_"$device".img"

rm -r $wDir
